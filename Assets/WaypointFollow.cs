﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class WaypointFollow : MonoBehaviour
{
    [SerializeField] WaypointCircuit circuit;
    //[SerializeField] GameObject[] waypoints;
    int currentWP = 0;

    [SerializeField] float speed = 2.5f;
    [SerializeField] float accuracy = 1.0f;
    [SerializeField] float rotSpeed = 1.0f;


    // Use this for initialization
    void Start()
    {
        //waypoints = GameObject.FindGameObjectsWithTag("waypoint");
        //Array.Reverse(waypoints);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (circuit.Waypoints.Length == 0) return;

        Vector3 lookAtGoal = new Vector3(
            circuit.Waypoints[currentWP].transform.position.x,
            transform.position.y,
            circuit.Waypoints[currentWP].transform.position.z
        );

        Vector3 direction = lookAtGoal - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);

        if (direction.magnitude < accuracy)
        {
            currentWP = currentWP >= circuit.Waypoints.Length - 1 ? 0 : ++currentWP;
            //++currentWP;
            //if (currentWP >= waypoints.Length) currentWP = 0;
        }
        transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
